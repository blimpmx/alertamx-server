#  vagrant box add trusty https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-i386-vagrant-disk1.box

sudo apt-get install -y postgresql-9.3-postgis-2.1

sudo cp /vagrant/provisioning/postgresql.conf /etc/postgresql/9.3/main/postgresql.conf
sudo cp /vagrant/provisioning/pg_hba.conf /etc/postgresql/9.3/main/pg_hba.conf

sudo service postgresql restart

sudo adduser --disabled-password --gecos "" alertamx
sudo -u postgres createuser alertamx -d -w
sudo -u postgres createdb alertamx
echo "ALTER USER alertamx PASSWORD 'alertamx';" | sudo -u postgres psql 
echo "CREATE EXTENSION postgis;" | sudo -u postgres psql alertamx
