
package models;

import com.google.publicalerts.cap.Area;
import com.google.publicalerts.cap.Info;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import play.db.jpa.Model;

/**
 *
 * @author rodolfo
 */
@Entity
public class MInfo extends Model {
    
    @ElementCollection
    public List<Info.Category> categories = new ArrayList<Info.Category>();
    
    public String event;
    
    public Info.Urgency urgency;
    
    public Info.Severity severity;
    
    public Info.Certainty certainty;
    
    @OneToMany(mappedBy = "info", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<MArea> areas = new ArrayList<MArea>();
    
    @ManyToOne(fetch = FetchType.LAZY)
    public MAlert alert;

    MInfo(Info info) {
        categories.addAll(info.getCategoryList());
        event = info.getEvent();
        urgency = info.getUrgency();
        severity = info.getSeverity();
        certainty = info.getCertainty();
        
        for (Area area : info.getAreaList()) {
            areas.add(new MArea(area));
        }
    }

    public MInfo() {
    }
        
    public void addArea(MArea area) {
        areas.add(area);
        area.info = this;
    }
    
}
