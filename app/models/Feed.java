package models;

import controllers.CRUD;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import play.data.validation.Required;
import play.db.jpa.Model;

/**
 *
 * @author rodolfo
 */
@Entity
public class Feed extends Model {
    
    @Required
    public String descripcion;
    
    @Required
    public String url;
    
    public boolean habilitado;
    
    @Temporal(TemporalType.TIMESTAMP)
    @CRUD.Exclude
    public Date ultimaActualizacion;

    @Override
    public String toString() {
        return descripcion;
    }
    
    
}
