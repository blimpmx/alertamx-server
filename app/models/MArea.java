package models;

import com.google.publicalerts.cap.Area;
import com.google.publicalerts.cap.Circle;
import com.google.publicalerts.cap.Point;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.util.GeometricShapeFactory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Type;
import play.Logger;
import play.db.jpa.Model;

/**
 *
 * @author rodolfo
 */
@Entity
public class MArea extends Model {

    public String areaDescription;

    @Column(name = "area", columnDefinition = "Geometry", nullable = true)

    @Type(type = "org.hibernatespatial.GeometryUserType")
    public Geometry area;

    @ManyToOne(fetch = FetchType.LAZY)
    public MInfo info;

    public MArea() {
    }

    public MArea(Area area) {
        areaDescription = area.getAreaDesc();

        List<Geometry> geometries = new ArrayList<Geometry>();

        GeometryFactory factory = new GeometryFactory();

        for (Circle circle : area.getCircleList()) {
            geometries.add(createCircle(circle));
        }

        for (com.google.publicalerts.cap.Polygon polygon : area.getPolygonList()) {
            Geometry geom = createPolygon(polygon, factory);
            if(geom != null) {
                geometries.add(geom);
            }
        }

        if (geometries.isEmpty()) {
            this.area = null;
        } else if (geometries.size() == 1) {
            this.area = geometries.get(0);
        } else {
            this.area = new GeometryCollection(geometries.toArray(new Geometry[]{}), factory);
        }
    }

    private Geometry createCircle(Circle circle) {
        GeometricShapeFactory gsf = new GeometricShapeFactory();
        gsf.setNumPoints(100);
        gsf.setCentre(new Coordinate(circle.getPoint().getLatitude(), circle.getPoint().getLatitude()));
        gsf.setSize(circle.getRadius() * 2);
        Polygon pcircle = gsf.createCircle();
        return pcircle;
    }

    private Geometry createPolygon(com.google.publicalerts.cap.Polygon polygon, GeometryFactory fact) {

        List<Coordinate> coordinates = new ArrayList<Coordinate>();
        for (Point point : polygon.getPointList()) {
            coordinates.add(new Coordinate(point.getLatitude(), point.getLatitude()));
        }

        if (coordinates.size() > 3) {
            LinearRing linear = fact.createLinearRing(coordinates.toArray(new Coordinate[]{}));
            Polygon poly = new Polygon(linear, null, fact);
            return poly;
        } else if (coordinates.size() >= 2) {
            LineString line = fact.createLineString(coordinates.toArray(new Coordinate[]{}));
            return line;
        } else {
            return null;
        }
    }

}
