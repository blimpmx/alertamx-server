package models;

import com.google.publicalerts.cap.Alert;
import com.google.publicalerts.cap.CapJsonBuilder;
import com.google.publicalerts.cap.CapUtil;
import com.google.publicalerts.cap.Info;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import play.db.jpa.Model;

/**
 *
 * @author rodolfo
 */
@Entity
public class MAlert extends Model {

    public String messageId;
    
    public String senderId;
    
    public Date sent;
    
    public boolean notificado;
    
    @Enumerated(EnumType.STRING)
    public Alert.Status status;
    
    @Enumerated(EnumType.STRING)
    public Alert.MsgType type;
    
    @Enumerated(EnumType.STRING)
    public Alert.Scope scope;
    
    @Column(columnDefinition = "TEXT")
    public String json;
    
    @OneToMany(mappedBy = "alert", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public List<MInfo> infos = new ArrayList<MInfo>();
    
    public final void addInfo(MInfo info) {
        infos.add(info);
        info.alert = this;
    }
    
    public MAlert() {
    }
    
    public MAlert(Alert alert) {
        messageId = alert.getIdentifier();
        senderId = alert.getSender();
        sent = CapUtil.toJavaDate(alert.getSent());
        status = alert.getStatus();
        type = alert.getMsgType();
        scope = alert.getScope();
        
        for (Info info : alert.getInfoList()) {
            addInfo(new MInfo(info));
        }
        
        CapJsonBuilder jsonBuilder = new CapJsonBuilder();
        json = jsonBuilder.toJson(alert);
    }
}
