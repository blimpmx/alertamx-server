package models;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import play.db.jpa.Model;

/**
 *
 * @author rodolfo
 */
@Entity
public class Dispositivo extends Model {
    
    public enum Tecnologia {
        ios, android;
    }
    
    public String pushToken;
    
    @Enumerated
    public Tecnologia tecnologia = Tecnologia.ios;
}
