package models;

import java.util.List;

/**
 *
 * @author rodolfo
 */
public final class MAlertSerializer {

    public static String serialize(List<MAlert> alerts) {
        StringBuilder sb = new StringBuilder("[");
        
        String separator = "";
        for (MAlert mAlert : alerts) {
            sb.append(separator);
            sb.append(mAlert.json);

            separator = ",";
        }
        
        sb.append("]");
        return sb.toString();
    }
    
}
