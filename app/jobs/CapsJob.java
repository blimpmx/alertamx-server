package jobs;

import akka.dispatchers.FeedDispatcher;
import com.relayrides.pushy.apns.ApnsEnvironment;
import com.relayrides.pushy.apns.FailedConnectionListener;
import com.relayrides.pushy.apns.PushManager;
import com.relayrides.pushy.apns.PushManagerConfiguration;
import com.relayrides.pushy.apns.RejectedNotificationListener;
import com.relayrides.pushy.apns.RejectedNotificationReason;
import com.relayrides.pushy.apns.util.ApnsPayloadBuilder;
import com.relayrides.pushy.apns.util.SSLContextUtil;
import com.relayrides.pushy.apns.util.SimpleApnsPushNotification;
import com.relayrides.pushy.apns.util.TokenUtil;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.List;
import javax.net.ssl.SSLHandshakeException;
import models.Feed;
import play.Logger;
import play.Play;
import play.jobs.Every;
import play.jobs.Job;

/**
 *
 * @author rodolfo
 */
@Every("10s")
public class CapsJob extends Job {

    static PushManager<SimpleApnsPushNotification> pushManager;

    @Override
    public void doJob() throws Exception {
        // trayendo feeds
        List<Feed> feeds = Feed.find("habilitado", true).fetch();
        for (Feed feed : feeds) {
            Logger.info("Lanzando dispatcher para %s", feed);
            FeedDispatcher feedDispatcher = new FeedDispatcher(feed);
            feedDispatcher.doProcessing();
        }

        //enviarMensajeIOS("2f9fcdcd795ba5a9a1eb11e7d4b60ce2b1517ee1d518c3b9e455a0ec1fb85009");
    }

    private void enviarMensajeIOS(String token) {
        Logger.info("Enviando Notificacion a %s", token);
        ApnsPayloadBuilder payloadBuilder = new ApnsPayloadBuilder();

        payloadBuilder.setAlertBody("alerta");
        payloadBuilder.setSoundFileName("default");
        payloadBuilder.setBadgeNumber(1);
        payloadBuilder.setContentAvailable(true);
        //payloadBuilder.addCustomProperty("idalert", alerta.messageId);

        final String payload = payloadBuilder.buildWithDefaultMaximumLength();
        try {
            byte[] tokenBytes = TokenUtil.tokenStringToByteArray(token);

            Logger.info("Logger tokens %s", TokenUtil.tokenBytesToString(tokenBytes));
            getPushManager().getQueue()
                    .put(new SimpleApnsPushNotification(tokenBytes, payload));
        } catch (Exception e) {
            e.printStackTrace();
        }

        //List<ApnsToken> tokens = alerta.communityOnly ? tokensComunidad : todosTokens;
    }

    protected PushManager<SimpleApnsPushNotification> getPushManager() {
        if (pushManager == null) {
            try {
                String password = Play.configuration.getProperty("apns.contrasena");
                Boolean produccion = Boolean.valueOf(
                        Play.configuration.getProperty("apns.produccion",
                                Boolean.FALSE.toString()));

                ApnsEnvironment apnsEnvironment = produccion ? ApnsEnvironment.getProductionEnvironment() : ApnsEnvironment.getSandboxEnvironment();
                Logger.info("Ambiente %s", apnsEnvironment.getApnsGatewayHost());

                pushManager = new PushManager<SimpleApnsPushNotification>(
                        apnsEnvironment,
                        SSLContextUtil.createDefaultSSLContext(Play.configuration.getProperty("apns.certificado"), password),
                        null, // Optional: custom event loop group
                        null, // Optional: custom ExecutorService for calling listeners
                        null, // Optional: custom BlockingQueue implementation
                        new PushManagerConfiguration(),
                        "ExamplePushManager");

                pushManager.registerRejectedNotificationListener(new MyRejectedNotificationListener());
                pushManager.registerFailedConnectionListener(new MyFailedConnectionListener());
                pushManager.start();
            } catch (KeyStoreException ex) {
                Logger.info(ex.getMessage());
            } catch (NoSuchAlgorithmException ex) {
                Logger.info(ex.getMessage());
            } catch (CertificateException ex) {
                Logger.info(ex.getMessage());
            } catch (UnrecoverableKeyException ex) {
                Logger.info(ex.getMessage());
            } catch (KeyManagementException ex) {
                Logger.info(ex.getMessage());
            } catch (IOException ex) {
                Logger.info(ex.getMessage());
            }
        }
        return pushManager;
    }

    private class MyRejectedNotificationListener implements RejectedNotificationListener<SimpleApnsPushNotification> {

        public void handleRejectedNotification(
                final PushManager<? extends SimpleApnsPushNotification> pushManager,
                final SimpleApnsPushNotification notification,
                final RejectedNotificationReason reason) {

            Logger.error("%s was rejected with rejection reason %s\n", notification, reason);
        }

    }

    private class MyFailedConnectionListener implements FailedConnectionListener<SimpleApnsPushNotification> {

        public void handleFailedConnection(
                final PushManager<? extends SimpleApnsPushNotification> pushManager,
                final Throwable cause) {

            
            Logger.error("error ", cause);
            if (cause instanceof SSLHandshakeException) {
                // This is probably a permanent failure, and we should shut down
                // the PushManager.
                Logger.error("handshake error");
            }
        }

    }

}
