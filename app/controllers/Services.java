package controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import models.Dispositivo;
import models.MAlert;
import models.MAlertSerializer;
import play.Logger;
import play.mvc.Controller;

/**
 *
 * @author rodolfo
 */
public class Services extends Controller {

    public static void alerts(Integer max) {
        if (max == null) {
            max = 30;
        }

        List<MAlert> alerts = MAlert.find(" order by sent desc").fetch(max);
        String alertsStr = MAlertSerializer.serialize(alerts);

        renderJSON(alertsStr);
    }

    public static void alert(String id) {
        MAlert alert = MAlert.find("messageId", id).first();
        notFoundIfNull(alert);

        renderJSON(alert.json);
    }

    public static void registraDispositivo(String token, String tecnologia) {
        notFoundIfNull(token);
        Map<String, Object> returnVal = new HashMap<String, Object>();

        Logger.info("Registrando dispositivo");

        Dispositivo dispositivo = Dispositivo.find("pushToken", token).first();

        Dispositivo.Tecnologia tec = Dispositivo.Tecnologia.ios;

        Logger.info("Peticion de registro %s", token);
        if (dispositivo == null) {
            dispositivo = new Dispositivo();
            dispositivo.pushToken = token;
            dispositivo.tecnologia = tec;
            dispositivo.save();

            returnVal.put("return", 200);
            Logger.info("Se registro dispositivo %s", token);
            renderJSON(returnVal);
        } else {
            Logger.info("Dispositivo ya registrado %s", token);
            returnVal.put("return", 400);
            renderJSON(returnVal);
        }
    }

}
