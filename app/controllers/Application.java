package controllers;

import models.MAlert;
import play.modules.paginate.ModelPaginator;
import play.mvc.*;


public class Application extends Controller {

    public static void index() {
        ModelPaginator paginator = new ModelPaginator(MAlert.class).orderBy("sent DESC");
        
        render(paginator);
    }

}