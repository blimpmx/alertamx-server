package akka.messages;

import models.MAlert;

/**
 *
 * @author rodolfo
 */
public class AlertMessage {

    public Long alertId;

    public AlertMessage(MAlert alert) {
        this.alertId = alert.id;
    }

}
