package akka.messages;

import models.Feed;


public class FeedMessage {

    public Long feedId;
    
    public FeedMessage(Feed feed) {
        this.feedId = feed.id;
    }
}
