package akka.actors;

import akka.actor.UntypedActor;
import akka.dispatchers.AlertDispatcher;
import akka.messages.FeedMessage;
import com.google.publicalerts.cap.Alert;
import com.google.publicalerts.cap.CapException;
import com.google.publicalerts.cap.CapJsonBuilder;
import com.google.publicalerts.cap.NotCapException;
import com.google.publicalerts.cap.feed.CapFeedParser;
import com.sun.syndication.io.FeedException;
import java.util.List;
import models.Feed;
import models.MAlert;
import org.xml.sax.InputSource;
import play.Logger;
import play.db.jpa.JPAPlugin;
import play.libs.WS;

public class FeedActor extends UntypedActor {

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof FeedMessage) {
            FeedMessage message = (FeedMessage) o;

            if (message.feedId != null) {
                JPAPlugin.startTx(false); // starting a non-readonly transaction
                Feed feed = Feed.findById(message.feedId);
                JPAPlugin.closeTx(false); // close the transaction. false means don't roll back

                if (feed != null) {
                    procesarFeed(feed);
                } else {
                    Logger.info("No result record was found for the id:" + message.feedId + ", check that the result record was actually saved.");
                }

            } else {
                Logger.info("No result.id was provided and so no calculation will be performed");
            }

        } else {
            unhandled(o);
        }
    }

    private void procesarFeed(Feed feed) {
        Logger.info("Procesando feed " + feed.descripcion);

        String url = feed.url;
        WS.HttpResponse res = WS.url(url).get();

        CapFeedParser capFeedParser = new CapFeedParser(false);
        CapJsonBuilder jsonBuilder = new CapJsonBuilder();
        List<Alert> alerts = null;
        try {
            InputSource inputSource = new InputSource(res.getStream());
            alerts = capFeedParser.parseAlerts(capFeedParser.parseFeed(inputSource));

            for (Alert alert : alerts) {
                JPAPlugin.startTx(true);
                MAlert current = MAlert.find("byMessageId", alert.getIdentifier()).first();
                JPAPlugin.closeTx(false); // close the transaction. false means don't roll back
                if (current == null) {
                    try {
                        JPAPlugin.startTx(false);
                        MAlert malert = new MAlert(alert);
                        malert.notificado = false;

                        malert.save();
                        malert.refresh();
                        JPAPlugin.closeTx(false); // close the transaction. false means don't roll back

                        AlertDispatcher dispatcher = new AlertDispatcher(malert);
                        dispatcher.doProcessing();
                        //Logger.info("json %s", json);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

        } catch (CapException e) {
            Logger.error("Error parseando feed");
            e.printStackTrace();
        } catch (NotCapException e) {
            Logger.error("Error parseando feed");
            e.printStackTrace();
        } catch (FeedException e) {
            Logger.error("Error parseando feed");
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            Logger.error("Error parseando feed");
            e.printStackTrace();
        }
    }

}
