package akka.actors;

import akka.actor.UntypedActor;
import akka.messages.AlertMessage;
import com.relayrides.pushy.apns.ApnsEnvironment;
import com.relayrides.pushy.apns.FailedConnectionListener;
import com.relayrides.pushy.apns.PushManager;
import com.relayrides.pushy.apns.PushManagerConfiguration;
import com.relayrides.pushy.apns.RejectedNotificationListener;
import com.relayrides.pushy.apns.RejectedNotificationReason;
import com.relayrides.pushy.apns.util.ApnsPayloadBuilder;
import com.relayrides.pushy.apns.util.MalformedTokenStringException;
import com.relayrides.pushy.apns.util.SSLContextUtil;
import com.relayrides.pushy.apns.util.SimpleApnsPushNotification;
import com.relayrides.pushy.apns.util.TokenUtil;
import java.io.IOException;
import java.net.URLDecoder;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.List;
import javax.net.ssl.SSLHandshakeException;
import models.Dispositivo;
import models.MAlert;
import play.Logger;
import play.Play;
import play.db.jpa.JPAPlugin;

/**
 *
 * @author rodolfo
 */
public class AlertActor extends UntypedActor {

    static PushManager<SimpleApnsPushNotification> pushManager;

    @Override
    public void onReceive(Object o) throws Exception {
        if (o instanceof AlertMessage) {
            AlertMessage message = (AlertMessage) o;

            if (message.alertId != null) {
                JPAPlugin.startTx(false); // starting a non-readonly transaction
                MAlert alert = MAlert.findById(message.alertId);

                if (alert != null) {
                    procesarAlert(alert);
                } else {
                    Logger.info("No result record was found for the id:" + message.alertId + ", check that the result record was actually saved.");
                }

                JPAPlugin.closeTx(false); // close the transaction. false means don't roll back
            } else {
                Logger.info("No result.id was provided and so no calculation will be performed");
            }

        } else {
            unhandled(o);
        }
    }

    @Override
    public void postStop() {
        Logger.info("Post stop");
    }

    @Override
    public void postRestart(Throwable reason) {
        Logger.info("Post restart");

    }

    private void procesarAlert(MAlert alert) {
        Logger.debug("Enviando alertas de %s", alert.messageId);
        List<Dispositivo> dispositivos = Dispositivo.findAll();

        for (Dispositivo dispositivo : dispositivos) {
            enviarMensajeIOS(dispositivo.pushToken, alert);
        }

    }

    private void enviarMensajeIOS(String token, MAlert alerta) {
        if (token == null || token.trim().isEmpty()) {
            Logger.error("Token nulo o vacio %s", token);
            return;
        }

        ApnsPayloadBuilder payloadBuilder = new ApnsPayloadBuilder();
        Logger.info("Enviando Notificacion a %s", token.toUpperCase());

        payloadBuilder.setAlertBody(alerta.infos.get(0).event);
        payloadBuilder.setSoundFileName("BeeDo");
        payloadBuilder.setBadgeNumber(1);
        payloadBuilder.setContentAvailable(true);
        payloadBuilder.addCustomProperty("idalert", alerta.messageId);

        final String payload = payloadBuilder.buildWithDefaultMaximumLength();
        try {
            byte[] tokenBytes = TokenUtil.tokenStringToByteArray(token.toUpperCase());

            Logger.info("Logger tokens %s", TokenUtil.tokenBytesToString(tokenBytes));

            getPushManager().getQueue()
                    .put(new SimpleApnsPushNotification(tokenBytes, payload));
        } catch (InterruptedException e) {
            Logger.error("Error %s", e);
        } catch (MalformedTokenStringException ex) {
            Logger.error("Error %s", ex);
        }
    }

    protected PushManager<SimpleApnsPushNotification> getPushManager() {

        Logger.info("getPushManager");
        if (pushManager == null) {
            try {
                String password = Play.configuration.getProperty("apns.contrasena");
                Boolean produccion = Boolean.valueOf(
                        Play.configuration.getProperty("apns.produccion",
                                Boolean.FALSE.toString()));

                ApnsEnvironment apnsEnvironment = produccion ? ApnsEnvironment.getProductionEnvironment() : ApnsEnvironment.getSandboxEnvironment();
                Logger.info("Ambiente %s", apnsEnvironment.getApnsGatewayHost());

                pushManager = new PushManager<SimpleApnsPushNotification>(
                        apnsEnvironment,
                        SSLContextUtil.createDefaultSSLContext(Play.classloader.getResource(
                                        Play.configuration.getProperty("apns.certificado")).openStream(), password),
                        null, // Optional: custom event loop group
                        null, // Optional: custom ExecutorService for calling listeners
                        null, // Optional: custom BlockingQueue implementation
                        new PushManagerConfiguration(),
                        "ExamplePushManager");

                pushManager.registerRejectedNotificationListener(new MyRejectedNotificationListener());
                pushManager.registerFailedConnectionListener(new MyFailedConnectionListener());
                pushManager.start();
            } catch (KeyStoreException ex) {
                Logger.info(ex.getMessage());
            } catch (NoSuchAlgorithmException ex) {
                Logger.info(ex.getMessage());
            } catch (CertificateException ex) {
                Logger.info(ex.getMessage());
            } catch (UnrecoverableKeyException ex) {
                Logger.info(ex.getMessage());
            } catch (KeyManagementException ex) {
                Logger.info(ex.getMessage());
            } catch (IOException ex) {
                Logger.info(ex.getMessage());
            }
        }

        Logger.info("pushmanager %s", pushManager);

        return pushManager;
    }

    private class MyRejectedNotificationListener implements RejectedNotificationListener<SimpleApnsPushNotification> {

        public void handleRejectedNotification(
                final PushManager<? extends SimpleApnsPushNotification> pushManager,
                final SimpleApnsPushNotification notification,
                final RejectedNotificationReason reason) {

            Logger.error("%s was rejected with rejection reason %s\n", notification, reason);
        }

    }

    private class MyFailedConnectionListener implements FailedConnectionListener<SimpleApnsPushNotification> {

        public void handleFailedConnection(
                final PushManager<? extends SimpleApnsPushNotification> pushManager,
                final Throwable cause) {

            Logger.error("error ", cause);
            if (cause instanceof SSLHandshakeException) {
                // This is probably a permanent failure, and we should shut down
                // the PushManager.
                Logger.error("handshake error");
            }
        }

    }
}
