package akka.dispatchers;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actors.AlertActor;
import akka.messages.AlertMessage;
import akka.routing.RoundRobinRouter;
import models.MAlert;
import system.AppContext;

/**
 *
 * @author rodolfo
 */
public class AlertDispatcher {

    public static ActorRef alertActor = AppContext.actorSystem.actorOf(new Props(AlertActor.class).withRouter(new RoundRobinRouter(4)), "AlertActor");

    private AlertMessage message;

    public AlertDispatcher(MAlert alert) {
        message = new AlertMessage(alert);
    }

    public void doProcessing() {
        alertActor.tell(message, alertActor);
    }

}
