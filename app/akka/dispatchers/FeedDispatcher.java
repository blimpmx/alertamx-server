package akka.dispatchers;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actors.FeedActor;
import akka.messages.FeedMessage;
import akka.routing.RoundRobinRouter;
import models.Feed;
import system.AppContext;


public class FeedDispatcher {

    public static ActorRef feedActor = AppContext.actorSystem.actorOf(new Props(FeedActor.class).withRouter(new RoundRobinRouter(4)), "FeedActor" );

    private FeedMessage message;

    public FeedDispatcher(Feed feed){
        message = new FeedMessage(feed);
    }

    public void doProcessing(){
        feedActor.tell(message, feedActor);
    }

}
