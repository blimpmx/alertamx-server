![Blimp.mx](http://blimp.mx/firmas/blimp.png)

# Agrupador de feeds CAP y notificador#

Este componente se encarga de obtener frecuentemente feeds cap y alertar a clientes móviles en caso de nuevas alertas. Los clientes móviles se suscriben y obtienen información de alertas a través de un servicio REST. El módulo de notificaciones no hace uso de ningún servicio de terceros. Además, provee una interfaz gráfica para el mantenimiento de feeds (Altas, Bajas, Cambios).

Este repositorio provee un script para la instalación de un sistema completo (base de datos + aplicación web). La base de datos corre sobre una máquina virtual por lo que se necesita Vagrant y VirtualBox en la máquina de desarrollo.

## Arquitectura ##

Esta aplicación provee una interfaz web para la gestión de feeds CAP, un módulo encargado del procesamiento de feeds y otro para el envío de notificaciones push. Tanto los módulos que procesan CAPs y envían notificaciones se desarrollando con el modelo de actores de Akka, lo que permite paralelismo y distribución de tareas. 

## Instalación ##

### Requerimientos ###
* Java 6
* Play 1.2.7 [(¿Cómo instalar?)](https://www.playframework.com/documentation/1.2.7/install)
* [Postgresql + postgis](http://postgis.net/)
* VirtualBox [¿Cómo instalar?](https://www.virtualbox.org/wiki/Downloads) 
* Vagrant [¿Cómo instalar?](https://docs.vagrantup.com/v2/installation/)
 

### Procedimiento de instalación ###
Clona este repositorio

`git clone https://bitbucket.org/blimpmx/alertamx-server.git`

Entra al directorio que se descargó

`cd alertamx-server`

Instalar la máquina virtual de referencia con Vagrant

`vagrant box add trusty https://cloud-images.ubuntu.com/vagrant/trusty/current/trusty-server-cloudimg-amd64-vagrant-disk1.box`

Levantar la máquina virtual. La primera vez que se ejecute se descargarán automáticamente todas las dependencias (postgres+postgis) y se configurará la base de datos del proyecto.

`vagrant up`

 Hacer que play descargue las dependencias

`play deps`

Ejecuta play

`play run`

Abre un navegador con la siguiente dirección http://localhost:11001. Se mostrará la lista de alertas recientes.

### Agregando feeds ###
Entra a  http://localhost:11001 y usa como usuario y contraseña la palabra *admin*

![Screen Shot 2014-09-17 at 3.16.48 PM.png](https://bitbucket.org/repo/qonM8M/images/2077810617-Screen%20Shot%202014-09-17%20at%203.16.48%20PM.png)

Aparecerá una lista con los feeds existentes. Para agregar un nuevo feed haz click en Add.

![Screen Shot 2014-09-17 at 3.19.09 PM.png](https://bitbucket.org/repo/qonM8M/images/2258003930-Screen%20Shot%202014-09-17%20at%203.19.09%20PM.png)

Captura el URL del feed CAP, su descripción y marcalo como habilitado.

![Screen Shot 2014-09-17 at 3.21.05 PM.png](https://bitbucket.org/repo/qonM8M/images/2744989448-Screen%20Shot%202014-09-17%20at%203.21.05%20PM.png)

El servidor procesa continuamente los feeds y envía notificaciones.

### Configuración ###
La configuración de la aplicación se hace en el archivo conf/application.conf

Es necesario configurar el certificado APNS en las variables que comienzan con APNS.

## Bibliotecas ##
* [Akka](http://akka.io/)
* [CAP Library](https://code.google.com/p/cap-library/)
* [Pushy](https://github.com/relayrides/pushy)

## Contacto ##

* Rodolfo Cartas rodolfo@blimp.mx

## Licencia ##
Copyright 2014 CARTAS DIAZ FLORES GURRIA LEAL MARQUEZ y ASOCIADOS SC

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.